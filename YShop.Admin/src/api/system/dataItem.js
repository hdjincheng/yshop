import request from '@/utils/request'

export function getItems(code){
    return request({
        url: '/api/data/items/' + code,
        method: 'get'
    })
}

export function getAction(code){
    return request({
        url: '/api/system-data-item/action/' + code,
        method: 'get'
    })
}

export function getCascades(code){
    return request({
        url: '/api/data/cascade/' + code,
        method: 'get'
    })
}


/**
 * 获取分类
 */
export function getCategorys(){
    return request({
        url: '/api/data/item/category/trees',
        method: 'get'
    })
}

/**
 * 查询数据项列表
 * @param {any} params 
 */
export function getList(params){
    return request({
        url: '/api/data/item/list',
        method: 'get',
        params
    })
}

/**
 * 获取单个
 * @param {string}} id 
 */
export function get(id){
    return request({
        url: '/api/data/item/' + id,
        method: 'get',
    })
}

/**
 * 更新
 * @param {*} data 
 */
export function update(data){
    return request({
        url: '/api/data/item',
        method: 'put',
        data:[data]
    })
}