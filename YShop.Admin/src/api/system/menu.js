import request from '@/utils/request'

export function getChildren(search){
    return request({
        url: '/api/menu/children/' + search,
        method: 'get'
    })
}

export function get(keyValue){
    return request({
        url: '/api/menu/' + keyValue,
        method: 'get'
    })
}

export function insert(data){
    return request({
        url: '/api/menu',
        method: 'post',
        data:data
    })
}

export function update(data){
    return request({
        url: '/api/menu',
        method: 'put',
        data:data
    })
}

export function remove(keyValue){
    return request({
        url: '/api/menu/' + keyValue,
        method: 'delete'
    })
}