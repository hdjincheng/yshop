import { getToken } from '@/utils/auth'

const state = {
    menus: [],
}
const mutations = {
}
const actions = {
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}