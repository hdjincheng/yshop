import './utils/init.js' // 其他初始化
import './api/api.js' // api 初始化
import './plugins/plugins.init.js' // 插件初始化
import './components/components.init' // 组件初始化

import Vue from 'vue'
import App from './App.vue'
import router from './router' 
import store from './store'
import './permission.js' // 权限管理
import './utils/mixin' // 全局混入

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
