import router from './router'
import store from './store'
import { Message } from 'element-ui'

router.beforeEach(async (to, from, next) => {
  // 开始进度条
  $y.progress.start();
  try {
    document.title = 'YBlog - ' + (to.name || to.path.replace('/', ''))
  }
  catch (e) {
    document.title = 'YBlog'
  }
  if(to.path == '/login') next()
  if (!store.state.route.registered) {
    try {
      var access = await store.dispatch('route/getAccessRoutes')
      router.addRoutes(access)
      next({ ...to, replace: true })
    } catch (e) {
      next('/login')
    }
  }
  next()
})


router.afterEach((to, from) => {
  store.dispatch('tabs/to', to)
  $y.progress.done(true)
})