import Vue from 'vue'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './element-variables.scss'

Vue.use(Element)


var { Notification } = Element
function notification(message, type = 'success') {
    Notification({
        title: message, type
    })
}

$y.notifier = {
    success: (message) => notification(message),
    warning: (message) => notification(message, 'warning'),
    info: (message) => notification(message, 'info'),
    error: (message) => notification(message, 'error'),
}


var { MessageBox } = Element
function messageBox(message, type, mode) {

    return new Promise((reslove, reject) => {
        MessageBox[mode](message, 'YBlog', {
            type,
            callback: res => {
                if (mode == 'confirm')
                    reslove(res == 'confirm')
                if (mode == 'prompt')
                    reslove(res.value)
            }
        })
    })
}

$y.confirm = {
    success: (message) => messageBox(message, 'success', 'confirm'),
    warning: (message) => messageBox(message, 'warning', 'confirm'),
    info: (message) => messageBox(message, 'info', 'confirm'),
    error: (message) => messageBox(message, 'error', 'confirm'),
}

$y.prompt = {
    success: (message) => messageBox(message, 'success', 'prompt'),
    warning: (message) => messageBox(message, 'warning', 'prompt'),
    info: (message) => messageBox(message, 'info', 'prompt'),
    error: (message) => messageBox(message, 'error', 'prompt'),
}