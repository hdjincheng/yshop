﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.DatabaseAccessor;
using Microsoft.AspNetCore.Mvc;
using YShop.Application.Contracts.System.Menus;
using YShop.Core.System;
using YShop.Core;
using Mapster;
using YShop.Core.Managers.Caches;
using YShop.Application.Contracts.Views;

namespace YShop.Application.System
{
    /// <summary>
    /// 菜单服务
    /// </summary>
    [ApiDescriptionSettings("System")]
    public class MenuService : CrudService<Menu, MenuDto, MenuDto, PageSearchDto, MenuDto, MenuDto>, IBaseService
    {
        /// <summary>
        /// 仓储
        /// </summary>
        private readonly IRepository<Menu> _repository;
        private readonly CacheManager _cacheManager;

        /// <summary>
        /// 全部菜单
        /// </summary>
        public override List<Menu> EntitiesCache => _cacheManager.GetOrCreate(CacheKeys.Menus, _ =>
        {
            return _repository.Include(s => s.Route, false).ToList();
        });
        public override string EntitiesCacheKey => CacheKeys.Menus;

        public MenuService(IRepository<Menu> repository,CacheManager cacheManager) : base(repository)
        {
            _repository = repository;
            _cacheManager = cacheManager;

        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="pageSearchDto"></param>
        /// <returns></returns>
        public override async Task<PagedList<MenuDto>> GetListAsync([FromQuery] PageSearchDto pageSearchDto)
        {
            return (await EntitiesCache.ToPageListAsync(pageSearchDto.PageIndex, pageSearchDto.PageSize)).Adapt<PagedList<MenuDto>>();
        }

        /// <summary>
        /// 获取子项
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public List<MenuDto> GetChildren(string parentId)
        {
            return EntitiesCache.Where(s => s.ParentId == parentId).Adapt<List<MenuDto>>();
        }

        /// <summary>
        /// 获取树形菜单
        /// </summary>
        /// <returns></returns>
        public List<MenuHasChildrenDto> GetTrees()
        {
            var menus = EntitiesCache.Adapt<List<MenuHasChildrenDto>>();
            var results = new List<MenuHasChildrenDto>();

            foreach(var menu in menus)
            {
                var parent = menus.FirstOrDefault(s => s.Id == menu.ParentId);

                if (parent != null)
                {
                    parent.AddChildren(menu);
                }
                else
                {
                    results.Add(menu);
                }
            }
            return results;
        }

        /// <summary>
        /// 获取联级数据
        /// </summary>
        /// <returns></returns>
        [SystemDataItem(code: "MenuCascaders", type: DataItemDetailsDataType.Cascade)]
        public List<Cascader> GetCascaders()
        {
            var config = new TypeAdapterConfig();
            config.ForType<Menu, Cascader>()
                .Map(dist => dist.Label, src => src.Name)
                .Map(d => d.Value, s => s.Id);

            var menus = EntitiesCache.Adapt<List<Cascader>>(config);
            var results = new List<Cascader>();

            foreach (var menu in menus)
            {
                var parent = menus.FirstOrDefault(s => s.Id == menu.ParentId);

                if (parent != null)
                {
                    parent.Children ??= new List<Cascader>();
                    parent.Children.Add(menu);
                }
                else
                {
                    results.Add(menu);
                }
            }
            return results;
        }
    }
}
