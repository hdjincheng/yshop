﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.Extensions.DependencyInjection;
using YShop.Core;

namespace YShop.EntityFramework.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseAccessor(options =>
            {
                options.AddDbPool<DefaultDbContext>($"{DbProvider.MySql}@8.0.20");
            }, "YShop.Database.Migrations");
        }
    }
}