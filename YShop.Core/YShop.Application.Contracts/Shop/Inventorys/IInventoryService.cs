﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.Shop.Inventorys
{
    public interface IInventoryService
    {
        Task<InventoryDto> GetBySkuIdAsync(string skuId);
        Task LockAsync(string skuId, decimal quantity);
        Task LockAsync(Dictionary<string, decimal> dictionaries);
    }
}
