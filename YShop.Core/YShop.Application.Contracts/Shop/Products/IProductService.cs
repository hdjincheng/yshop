﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.Shop.Products
{
    public interface IProductService
    {
        Task<PagedList<ProductDto>> GetPageList(ProductPageSearchDto search);
        Task<ProductDto> GetAsync(string id);
        Task SaveFormAsync(ProductDto productDto);

    }
}
