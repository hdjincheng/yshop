﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.Shop
{
    /// <summary>
    /// 产品明细(SKU)
    /// </summary>
    public class ProductDetailDto : BaseEntityDto
    {
        /// <summary>
        /// 产品ID
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// SKU编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 内部价
        /// </summary>
        public decimal InsidePrice { get; set; }
        /// <summary>
        /// 图片（如果没有，则默认使用主图）
        /// </summary>
        public string Images { get; set; }
        /// <summary>
        /// 选项
        /// </summary>
        public List<string> Options { get; set; }
    }
}
