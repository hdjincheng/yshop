﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YShop.Application.Contracts.System.Resources;
using YShop.Core.System;

namespace YShop.Application.Contracts.Shop
{
    /// <summary>
    /// 产品
    /// </summary>
    public class ProductDto:BaseEntityDto
    {
        /// <summary>
        /// 产品编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 子名称
        /// </summary>
        public string SubName { get; set; }
        /// <summary>
        /// 上架
        /// </summary>
        public bool Saleable { get; set; }
        /// <summary>
        /// 产品描述（富文本）
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 商品分类ID
        /// </summary>
        public string CategroyId { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price => Details.FirstOrDefault()?.Price ?? 0;
        /// <summary>
        /// 主图
        /// </summary>
        public string Picture => Images.FirstOrDefault()?.Url;
        /// <summary>
        /// 相册
        /// </summary>
        public List<ResourceDto> Images { get; set; }
        /// <summary>
        /// 产品选项
        /// 示例：{颜色:[黑、红、绿],尺寸:[S、M、L、XL、XXL]}
        /// </summary>
        public List<ProductAttributeDto> Attributes { get; set; }
        /// <summary>
        /// 产品明细(SKU)
        /// </summary>
        public List<ProductDetailDto> Details { get; set; }

    }
    /// <summary>
    /// 产品属性选项
    /// </summary>
    public class ProductAttributeDto : BaseEntityDto
    {
        public string ProductId { get; set; }
        public string AttributeId { get; set; }
        public string AttributeDetailId { get; set; }
    }

}
