﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.Shop.Products
{
    public class ProductPageSearchDto:PageSearchDto
    {
        /// <summary>
        /// 分类ID
        /// </summary>
        public string CategroyId { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 子名称
        /// </summary>
        public string SubName { get; set; }
        /// <summary>
        /// 上架
        /// </summary>
        public bool Saleable { get; set; }
    }
}
