﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.System.Resources
{
    /// <summary>
    /// 资源
    /// </summary>
    public class ResourceDto:BaseEntityDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 本地存储文件名
        /// </summary>
        public string StorageName { get; set; }
        /// <summary>
        /// 扩展名
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 大小
        /// </summary>
        public long Size { get; set; }
        /// <summary>
        /// 位置
        /// </summary>
        public int Location { get; set; }
        /// <summary>
        /// URL
        /// </summary>
        public string Url => $"{Path}/{StorageName}";
    }
}
