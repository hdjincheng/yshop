﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.System.Routes
{
    public class RouteHasChildrenDto : RouteDto
    {
        /// <summary>
        /// 子项
        /// </summary>
        public List<RouteDto> Children { get; set; } = new List<RouteDto>();
    }
}
 