﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Application.Contracts.System.DataItems
{
    /// <summary>
    /// 数据项明细
    /// </summary>
    public class DataItemDetailDto : BaseEntityDto
    {
        /// <summary>
        /// 数据项Id
        /// </summary>
        public string DataItemId { get; set; }
        /// <summary>
        /// 显式名
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// 隐式值
        /// </summary>
        public dynamic Value { get; set; }
        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName { get; set; }
        /// <summary>
        /// 额外的参数
        /// </summary>
        public dynamic Params { get; set; }
    }
}
