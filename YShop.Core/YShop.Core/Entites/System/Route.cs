﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace YShop.Core.System
{
    /// <summary>
    /// 路由
    /// </summary>
    [Table("Sys_Route")]
    public class Route : BaseEntity, IEntitySeedData<Route>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 重定向
        /// </summary>
        public string Redirect { get; set; }
        /// <summary>
        /// 组件
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// 元信息
        /// </summary>
        public Meta Meta { get; set; }

        public IEnumerable<Route> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<Route>
            {
                #region Home
                new Route{ Id = "0",Name = "Home",Path = "/",Redirect = "desktop",Component = "Home" },
	            #endregion

                #region 桌面
                new Route{ Id = "5ff0c7ee-c953-f494-00b4-329a422c0eee",Name = "桌面",Path = "/desktop",Component = "system/desktop" },
	            #endregion

                #region 菜单
                new Route{ Id = "5ff05630-b33d-a7c8-0014-effb6f4d89ab",Name = "菜单",Path = "/menu",Component = "system/menu/index" ,Meta = new Meta{ Name = "菜单",KeepAlive = true } },
                new Route{ Id = "5ff0c60b-7e5e-4fc8-009b-23a92e481348",Path = "/menu/form",Component = "system/menu/form" },
                #endregion
                
                #region 数据管理
                new Route{ Id = "5ff23e0e-8f52-f338-00b5-894b4613aac7",Name = "数据",Path = "/data",Component = "system/data/index" ,Meta = new Meta{ Name = "数据", KeepAlive = true }},
                new Route{ Id = "5ff23e2c-8f52-f338-00b5-894c7c07e767",Name = "数据Form",Path = "/data/form",Component = "system/data/form" },
	            #endregion

                #region 示例
                new Route{ Id = "5ff0c7fd-c953-f494-00b4-329b51a095c2",Path = "/sample", Component = "sample/index",Meta = new Meta{ Name = "示例",KeepAlive = true } },
                new Route{ Id = "5ff0c80d-c953-f494-00b4-329c6acae8cf",Name = "示例Form",Path = "/sample/form",Component = "system/form" },

                #endregion

                #region 系统开发
                new Route { Id = "6004ad61-436d-2cac-00f3-016d2eeb451a",Name = "代码生成器",Path = "/development/codeGenerator",Component ="development/codeGenerator/index",Meta = new Meta{ Name = "代码生成器",KeepAlive = true} },
                new Route { Id = "6004aefe-436d-2cac-00f3-016f0bae52ef",Name = "数据库管理",Path = "/development/database",Component = "development/database/index",Meta = new Meta{ Name = "数据库管理",KeepAlive = true} },
                new Route { Id = "6032db4e-1281-068c-002a-de3840bef1b5",Name = "其他",Path = "/development/other",Component = "development/other/index",Meta = new Meta{ Name = "其他",KeepAlive = true} },
                #endregion

                #region 商城管理
                new Route{ Id = "6032da9c-cf82-c8a8-00dd-939527afcbdc",Path = "/product", Component = "product/index",Meta = new Meta{ Name = "商品",KeepAlive = true } },
                new Route{ Id = "6032daa9-cf82-c8a8-00dd-93962709141e",Name = "商品Form",Path = "/product/form",Component = "product/form" },

                new Route{ Id = "6059063a-5def-4ebf-00c2-fbb962e274e4",Path = "/productcategroy", Component = "productCategroy/index",Meta = new Meta{ Name = "商品分类",KeepAlive = true } },
                new Route{ Id = "60590687-5def-4ebf-00c2-fbba357e90f9",Name = "商品分类Form",Path = "/productcategroy/form",Component = "productCategroy/form" },

                new Route{ Id = "6032dab0-cf82-c8a8-00dd-93977c537d9f",Path = "/order", Component = "order/index",Meta = new Meta{ Name = "订单",KeepAlive = true } },
                new Route{ Id = "6032dab9-cf82-c8a8-00dd-9398044fc160",Name = "订单Form",Path = "/order/form",Component = "order/form" },

                new Route{ Id = "6032dac1-cf82-c8a8-00dd-93993e91bdc7",Path = "/inventory", Component = "inventory/index",Meta = new Meta{ Name = "库存",KeepAlive = true } },
                new Route{ Id = "6032dac8-cf82-c8a8-00dd-939a616e0907",Name = "库存Form",Path = "/inventory/form",Component = "inventory/form" },

                new Route{ Id = "6032dad2-cf82-c8a8-00dd-939b29863f81",Path = "/payment", Component = "payment/index",Meta = new Meta{ Name = "支付",KeepAlive = true } },
                new Route{ Id = "6032dadb-cf82-c8a8-00dd-939c70e5cb41",Name = "支付Form",Path = "/payment/form",Component = "payment/form" },

                new Route { Id = "60435bb1-99fc-0d9c-00db-80f25a6c73ad",Path = "/product/attribute",Component = "productAttribute/index" ,Meta = new Meta { Name = "商品属性",KeepAlive = true  } },
                new Route{ Id = "60454b03-9984-3448-001a-bd85562dc9ab",Name = "商品属性Form",Path = "/product/attribute/form",Component = "productAttribute/form" },

	            #endregion

            };
        }
    }

    /// <summary>
    /// 元信息
    /// </summary>
    [Table("Sys_Meta")]
    public class Meta : BaseEntity
    {
        /// <summary>
        /// 路由ID
        /// </summary>
        public string RouteId { get; set; }
        /// <summary>
        /// 保持生机（缓存标志）
        /// </summary>
        public bool KeepAlive { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
