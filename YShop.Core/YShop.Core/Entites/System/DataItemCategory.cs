﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Furion;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YShop.Core.Managers.Caches;

namespace YShop.Core.System
{
    /// <summary>
    /// 数据项分类
    /// </summary>
    [Table("Sys_DataItemCategory")]
    public class DataItemCategory : BaseEntity, IEntitySeedData<DataItemCategory>,IModelBuilderFilter
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 父级Id
        /// 最顶级的父Id为 '0'
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 左节点
        /// </summary>
        public int Lft { get; set; }
        /// <summary>
        /// 右节点
        /// </summary>
        public int Rgt { get; set; }
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<DataItemCategory> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<DataItemCategory>
            {
                new DataItemCategory{ Id = "5ff4dd74-2999-3150-0090-c54313cdb591", Name = "系统管理", ParentId = "0", Lft = 0, Rgt = 5 },
                new DataItemCategory{ Id = "5ff4ddc4-2999-3150-0090-c54458c0c811", Name = "功能", ParentId = "5ff4dd74-2999-3150-0090-c54313cdb591", Lft = 1, Rgt = 2 },
                new DataItemCategory{ Id = "5ff4de5d-2999-3150-0090-c545721b25f4", Name = "设置", ParentId = "5ff4dd74-2999-3150-0090-c54313cdb591", Lft = 3, Rgt = 4 },
            };
        }

        public void OnCreating(ModelBuilder modelBuilder, EntityTypeBuilder entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            //App.GetService<CacheManager>().Remove(CacheKeys.DataItems);
        }
    }
}
