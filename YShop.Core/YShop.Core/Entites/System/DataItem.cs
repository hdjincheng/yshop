﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YShop.Core.Managers.Caches;

namespace YShop.Core.System
{
    /// <summary>
    /// 数据项
    /// </summary>
    [Table("Sys_DataItem")]
    public class DataItem : BaseEntity,IEntitySeedData<DataItem>,IModelBuilderFilter
    {
        /// <summary>
        /// 分类ID
        /// </summary>
        public string CategoryId { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public DataItemType Type { get; set; }
        /// <summary>
        /// 允许修改
        /// </summary>
        public bool EnableChanges { get; set; } = true;
        /// <summary>
        /// 明细项数据类型
        /// </summary>
        public DataItemDetailsDataType? DetailsDataType { get; set; }
        /// <summary>
        /// 明细项
        /// </summary>
        public List<DataItemDetail> Details { get; set; }

        public IEnumerable<DataItem> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<DataItem>
            {
                new DataItem{ CategoryId = "5ff4ddc4-2999-3150-0090-c54458c0c811",Id = "5ff4ded3-2999-3150-0090-c54609c792e1",Code = "MenuType",Name = "菜单类型",Type = DataItemType.Dict,DetailsDataType = DataItemDetailsDataType.List },
                new DataItem{ CategoryId = "5ff4ddc4-2999-3150-0090-c54458c0c811",Id = "5ff4df02-2999-3150-0090-c54743ae8369",Code = "DataItemType",Name = "数据项类型",Type = DataItemType.Dict,DetailsDataType = DataItemDetailsDataType.List },
            };
        }

        public void OnCreating(ModelBuilder modelBuilder, EntityTypeBuilder entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            //App.GetService<CacheManager>().Remove(CacheKeys.DataItems);
        }
    }

}
