﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Furion.DatabaseAccessor;
using Furion.DependencyInjection;

namespace YShop.Core
{
    /// <summary>
    /// 基类实体
    /// </summary>
    [SkipScan]
    public class BaseEntity : OnlyPrimaryEntity, IBaseEntity, ICreateAduitEntity, IDeleteAduitEntity, IUpdateAuditEntity
    {
        public BaseEntity()
        {
            this.IsDeleted = false;
        }

        /// <summary>
        /// 创建用户主键
        /// </summary>
        public Guid? CreateUserId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        //public bool IsEnabled { get; set; } = true;
        /// <summary>
        /// 是否删除
        /// </summary>
        [FakeDelete(true)]
        public bool? IsDeleted { get; set; } = false;
        /// <summary>
        /// 删除用户主键
        /// </summary>
        public Guid? DeleteUserId { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 修改用户主键
        /// </summary>
        public Guid? UpdateUserId { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 排序码
        /// </summary>
        public int SortCode { get; set; }
    }
}
