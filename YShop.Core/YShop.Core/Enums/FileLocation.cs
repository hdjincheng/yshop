﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core
{
    public enum FileLocation
    {
        /// <summary>
        /// 阿里云
        /// </summary>
        AliCloud,
        /// <summary>
        /// 腾讯云
        /// </summary>
        TencentCloud,
        /// <summary>
        /// 本地
        /// </summary>
        Local
    }
}
