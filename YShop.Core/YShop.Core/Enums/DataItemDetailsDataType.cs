﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YShop.Core
{
    /// <summary>
    /// 数据项明细数据类型
    /// </summary>
    public enum DataItemDetailsDataType
    {
        // 列表
        List,
        // 级联
        Cascade
    }
}
