import Vue from 'vue';
const settings = {
	// baseUrl: "https://api.yell.run",
	baseUrl: "https://localhost:5001",
	uploadUrl:'/api/file/upload',
}
Vue.prototype.settings = settings
module.exports = settings