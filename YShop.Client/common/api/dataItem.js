import { request } from '../request.js'

export function getAction(code){
    return request({
        url: '/api/system-data-item/action/' + code,
        method: 'get'
    })
}