import { request } from '../request.js'

export function getList(search){
	return request({
		url:'/api/product-categroy/list',
		method:'get',
		params:search
	})
}